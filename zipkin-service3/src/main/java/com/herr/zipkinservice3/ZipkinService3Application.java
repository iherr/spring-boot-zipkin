package com.herr.zipkinservice3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class ZipkinService3Application {

    public static void main(String[] args) {
        SpringApplication.run(ZipkinService3Application.class, args);
    }

}
