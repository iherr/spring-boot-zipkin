Zipkin是Twitter公司开源的一款调用追踪中心，用来收集分布式系统每个组件所花费的调用时长，并通过图形化界面展示整个调用链依赖关系，解决微服务架构中的延迟问题。

Zipkin参照Google的Dapper论文设计思想来开发。

[『官网传送门』](https://zipkin.io)

[『源码传送门』](https://github.com/openzipkin/zipkin)

Dapper的几个概念：

* Span，基本工作单元，调用一个组件所经历的一段过程，从请求组件开始到组件响应为止。
* Trace，追踪链路，指的客户端发出请求，知道完成整个内部调用的全部过程，可能包含多个Span。
* Reporter，将Span和Trace产生的追踪数据需要从客户端推送到zipkin的collector，官网包含了大量实现Reporter的代码库，可通过HTTP、Kafka（Apache开源）和Scribe（Facebook开源）等进行传输。
* Annotation，注解，用来记录请求特定事件相关信息(例如时间)，通常包含四个注解信息：1、cs - ClientStart,表示客户端发起请求。2、sr - Server Receive,表示服务端收到请求。3、ss - Server Send,表示服务端完成处理，并将结果发送给客户端。4、cr - Client Received,表示客户端获取到服务端返回信息。
* Collector，会对一个到来的被trace的数据（span）进行验证、存储并设置索引。
* Storage，存储，zipkin默认的存储方式为in-memory，即不会进行持久化操作。如果想进行收集数据的持久化，可以存储数据在Cassandra、ElasticSearch和MySQL中。
* API，提供了一个用于查找和检索跟踪的简单JSON API，此API的主要使用者是WebUI。
* WebUI，展示页面。