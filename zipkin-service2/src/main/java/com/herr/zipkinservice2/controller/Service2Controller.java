package com.herr.zipkinservice2.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.Random;

@RestController
public class Service2Controller {

    @Autowired
    private RestTemplate restTemplate;

    @GetMapping("/service2")
    public String service2() {
//        return "herr";
        try {
            Thread.sleep(new Random().nextInt(1000));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return restTemplate.getForObject("http://localhost:8083/service3", String.class);
    }
}
